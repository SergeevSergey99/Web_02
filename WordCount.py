import re
from collections import Counter
file_name = input()
f = open(file_name)
text = f.read()
print(text)

text = re.sub(r'[^\w\s]',' ',text)
text = re.sub(r'\n',' ',text)
text = text.lower()
text = text.split(" ")
print(text)
c = Counter(text)
print(c)
f.close()
